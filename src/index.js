import React from "react";
import ReactDOM from "react-dom";
import MainApp from "./ui/initial_page/Main";

ReactDOM.render(<MainApp />, document.getElementById("app"));

if (module.hot) {
  // enables hot module replacement if plugin is installed
  module.hot.accept();
}
